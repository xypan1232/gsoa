#!/bin/bash

set -o errexit

thisDir=$(dirname $0)

# Required parameters:
dataFilePath="$1"
classFilePath="$2"
gmtFilePaths="$3" # Separate multiple paths by a comma, wildcards are OK
outMetricsFilePath="$4"
# Optional parameters:
numCores="$5"
outPredictionsFilePath="$6"
numRandomIterations="$7"
rowsToExclude="$8" # Should be a comma separated list of row identifiers
numFolds="$9"
classificationAlgorithm="${10}"
permuteSeed="${11}"

if [ "$dataFilePath" == "" ]
then
  echo "A data file path must be specified (1st parameter)."
  exit 1
fi
if [ "$classFilePath" == "" ]
then
  echo "A class file path must be specified (2nd parameter)."
  exit 1
fi
if [ "$gmtFilePaths" == "" ]
then
  echo "A .gmt file path must be specified (3rd parameter)."
  exit 1
fi
if [ "$outMetricsFilePath" == "" ]
then
  echo "An output file path must be specified (4th parameter)."
  exit 1
fi

if [ "$numCores" == "" ]
then
  numCores=$(python code/PrintNumCpus.py)
fi
if [ "$numRandomIterations" == "" ]
then
  numRandomIterations=100
fi
if [ "$numFolds" == "" ]
then
  numFolds=5
fi
if [ "$classificationAlgorithm" == "" ]
then
  classificationAlgorithm=svmrbf
fi

tmpDir=$thisDir/../Temp
outDataDir=$tmpDir/gsoa/GeneSetData
outResultsDir=$tmpDir/gsoa/GeneSetResults
outPredictionsDir=$tmpDir/gsoa/GeneSetPredictions
outRandomDataDir=$tmpDir/gsoa/GeneSetRandomData
outRandomResultsDir=$tmpDir/gsoa/GeneSetRandomResults
outRandomPredictionsDir=$tmpDir/gsoa/GeneSetRandomPredictions
outPValueResultsDir=$tmpDir/gsoa/GeneSetPValueResults
outTargetClassFilePath=$tmpDir/gsoa/targetClass
commandScript=$tmpDir/gsoa/predict_these

if [ -f $outMetricsFilePath ]
then
  echo Results already exist at $outMetricsFilePath, so this will not be reprocessed.
  exit
else
  echo Getting ready to process $outMetricsFilePath
fi

mkdir -p $tmpDir
$thisDir/cleanup

# Clean up when this script ends
trap 'source $thisDir/cleanup' TERM INT EXIT

mkdir -p $outDataDir $outResultsDir $outPredictionsDir $outRandomDataDir $outRandomResultsDir $outRandomPredictionsDir $outPValueResultsDir $(dirname $outMetricsFilePath)

python $thisDir/../code/PrepareToRun.py "$gmtFilePaths" "$classFilePath" "$dataFilePath" $numRandomIterations "$rowsToExclude" $outDataDir $outResultsDir $outPredictionsDir $outRandomDataDir $outRandomResultsDir $outRandomPredictionsDir "python $thisDir/../code/Predict.py" $commandScript $numFolds $classificationAlgorithm "$permuteSeed" $outTargetClassFilePath

#chmod 777 $commandScript
echo Will execute across $numCores processing cores
parallel -a $commandScript --ungroup --max-procs $numCores --halt-on-error 1

python $thisDir/../code/CalculateEmpiricalPValues.py $outDataDir $outResultsDir $outRandomResultsDir $outPValueResultsDir
python $thisDir/../code/CreateFinalMetricsOutput.py "$outResultsDir/*" "$outPValueResultsDir/*" $numRandomIterations $tmpDir/Output.txt

headerText=$(echo "dataFilePath=$dataFilePath classFilePath=$classFilePath gmtFilePaths=$gmtFilePaths numCores=$numCores numRandomIterations=$numRandomIterations rowsToExclude=$rowsToExclude numFolds=$numFolds classificationAlgorithm=$classificationAlgorithm permuteSeed=$permuteSeed date=$(date) version=$(date -r $0)")

echo "# $headerText" > $tmpDir/OutputHeader.txt
cat $tmpDir/OutputHeader.txt $tmpDir/Output.txt > $outMetricsFilePath

if [ "$outPredictionsFilePath" != "" ]
then
  python $thisDir/../code/BuildMatrixFile.py "$outPredictionsDir/*" $classFilePath $tmpDir/Predictions.txt
  targetClass=$(cat $outTargetClassFilePath)
  echo "# targetClass=$targetClass $headerText" > $tmpDir/OutputHeader.txt
  cat $tmpDir/OutputHeader.txt $tmpDir/Predictions.txt > $outPredictionsFilePath
fi

rm -rf $tmpDir

echo See results summary in ${outMetricsFilePath}.
