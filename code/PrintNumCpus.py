#import psutil
import multiprocessing

#num = psutil.NUM_CPUS
#num = psutil.cpu_count(logical=False)
num = multiprocessing.cpu_count()

if num <= 2:
    print num
    exit(0)
if num <= 8:
    print num - 1
    exit(0)
if num <= 16:
    print num - 2
    exit(0)
if num <= 32:
    print num - 4
    exit(0)

print int(float(num) * 0.9)
