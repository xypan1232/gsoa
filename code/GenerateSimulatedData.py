import os, sys, glob, random
import numpy
from sklearn.datasets import *
from sklearn.svm import *
from sklearn.feature_selection import RFE
from sklearn import preprocessing
from random import randint

def generateData(numSamples, weights, outDataFilePath, outClassFilePath, outGeneSetFilePath):
    numRandomGenes = 19800
    numInformativeGenes = 25
    numRedundantGenes = 175

    # Simulate data using scikit-learn module
    # See http://scikit-learn.org/stable/modules/generated/sklearn.datasets.make_classification.html

    X, y = make_classification(n_samples=numSamples, n_features=(numRandomGenes + numInformativeGenes + numRedundantGenes), n_informative=numInformativeGenes, n_redundant=numRedundantGenes, n_repeated=0, n_classes=2, n_clusters_per_class=1, weights=weights, random_state=1, shuffle=False, flip_y=0.0)
    X = preprocessing.scale(X)

    outDataFile = open(outDataFilePath, 'w')

    # Write file header
    outDataFile.write("\t".join([""] + ["Sample%i" % i for i in range(1, numSamples + 1)]) + "\n")

    # Write values to file for each sample
    i = 1
    for row in numpy.transpose(X):
        outDataFile.write("\t".join(["Gene%i" % i] + [str(x) for x in row]) + "\n")
        i += 1
    outDataFile.close()

    informativeGenes = ["Gene%i" % i for i in range(1, (numInformativeGenes + numRedundantGenes + 1))]
    randomGenes = ["Gene%i" % i for i in range((numInformativeGenes + numRedundantGenes + 1), (numRandomGenes + numInformativeGenes + numRedundantGenes + 1))]

    # Save class information to file
    outClassFile = open(outClassFilePath, 'w')
    for i in range(numSamples):
        outClassFile.write("Sample%i\tClass%i\n" % (i + 1, y[i]))
    outClassFile.close()

    # Create GMT file
    random.seed(0)
    randomSeeds = [random.randrange(1, 1000000) for i in range(10000)]

    outGeneSetFile = open(outGeneSetFilePath, 'w')

    for repetition in range(1, 11):
        for numGenes in range(25, 301, 25):
            for numSignal in range(5, 51, 5):
                if numSignal > numGenes:
                    continue

                numRandom = numGenes - numSignal

                signalGenes = list(informativeGenes)
                random.seed(randomSeeds.pop())
                random.shuffle(signalGenes)
                signalGenes = signalGenes[:numSignal]

                randomGenes2 = list(randomGenes)
                random.seed(randomSeeds.pop())
                random.shuffle(randomGenes2)

                geneSetName = "Random%i_Signal%i_Repetition%i" % (numRandom, numSignal, repetition)
                outGeneSetFile.write("\t".join([geneSetName, geneSetName] + sorted(signalGenes + randomGenes2[:numRandom])) + "\n")

                geneSetName = "Random%i_Signal0_Repetition%i_%i" % (numRandom + numSignal, repetition, numSignal)
                outGeneSetFile.write("\t".join([geneSetName, geneSetName] + sorted(randomGenes2[:(numRandom + numSignal)])) + "\n")

    outGeneSetFile.close()

def selectInformativeFeatures(X, y, num_to_select):
    svc = SVC(kernel="linear", C=1)
    rfe = RFE(estimator=svc, n_features_to_select=num_to_select, step=0.1)
    rfe.fit(X, y)

    return rfe.support_

generateData(numSamples=100, weights=[0.50], outDataFilePath = "Simulation/Data_Main.txt", outClassFilePath="Simulation/Classes_Main.txt", outGeneSetFilePath="Simulation/GeneSets_Main.gmt")
generateData(numSamples=100, weights=[0.10, 0.90], outDataFilePath = "Simulation/Data_Unbalanced.txt", outClassFilePath="Simulation/Classes_Unbalanced.txt", outGeneSetFilePath="Simulation/GeneSets_Unbalanced.gmt")
