import os, sys, glob

inFilePattern = sys.argv[1]

for inFilePath in glob.glob(inFilePattern):
    for line in file(inFilePath):
        print line.rstrip()
