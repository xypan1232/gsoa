import os, sys, glob, math
import numpy
from utilities import *

inAUCFilePattern = sys.argv[1]
inPValueFilePattern = sys.argv[2]
numRandomIterations = float(sys.argv[3])
outFilePath = sys.argv[4]

def readValueFromFile(filePath):
    inFile = open(filePath)
    inFile.readline() # remove header line
    value = inFile.readline().rstrip()
    inFile.close()

    return value

numDecimalPlaces = int(math.ceil(math.log(numRandomIterations, 10)))

aucDict = {}
for filePath in glob.glob(inAUCFilePattern):
    aucDict[os.path.basename(filePath)] = float(readValueFromFile(filePath))

pValueMatrix = []
pValues = []
for filePath in glob.glob(inPValueFilePattern):
    pValue = float(readValueFromFile(filePath))
    pValueMatrix.append([os.path.basename(filePath), pValue])
    pValues.append(pValue)
pValueMatrix = sortMatrix(pValueMatrix, 1)

ranks = rankSmart([x[1] for x in pValueMatrix], ties="min")

outFormat = "%s\t%.6f\t%." + str(numDecimalPlaces) + "f\t%i\n"

outFile = open(outFilePath, 'w')
outFile.write("Gene Set\tAUC\tp-value\tRank\n")
for i in range(len(pValueMatrix)):
    geneSet = pValueMatrix[i][0]
    outFile.write(outFormat % (geneSet, aucDict[geneSet], pValueMatrix[i][1], int(ranks[i]) + 1))
outFile.close()
