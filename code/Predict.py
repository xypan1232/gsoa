import os, sys, glob
from operator import itemgetter, attrgetter
import numpy
import scipy
import sklearn.svm
import sklearn.preprocessing
import sklearn.cross_validation
import sklearn.metrics
import sklearn.naive_bayes
import sklearn.neighbors
import sklearn.tree
import sklearn.dummy
import sklearn.ensemble
from sklearn import grid_search
from utilities import *

description = sys.argv[1]
dataFilePath = sys.argv[2]
outMetricFilePath = sys.argv[3]
outPredictionsFilePath = sys.argv[4]
numFolds = sys.argv[5]
classificationAlgorithm = sys.argv[6]
targetClass = sys.argv[7]

def getClassifier():
    if classificationAlgorithm.startswith("svm"):
        classificationAlgorithmItems = classificationAlgorithm.replace("svm", "").split("_")

        kernel = classificationAlgorithmItems[0]

        # You can specify values for C and gamma. For example: svmrbf_0.1_1.0 for specific parameters or svmrbf_auto to auto tune the parameters.

        if len(classificationAlgorithmItems) == 2:
            if classificationAlgorithmItems[1] != "auto":
                print "Invalid classifier: %s" % classificationAlgorithm

            parameters = {'C':[0.01, 0.1, 1.0, 10.0], 'gamma':[0.0, 0.1, 1.0, 10.0]}
            algorithm = sklearn.svm.SVC(kernel=kernel, probability=True, cache_size=200, verbose=False, random_state=0, class_weight="auto")
            algorithm = grid_search.GridSearchCV(algorithm, parameters)

            return algorithm

        # Defaults
        C = 1.0
        gamma = 0.0
        if len(classificationAlgorithmItems) == 3:
            C = float(classificationAlgorithmItems[1])
            gamma = float(classificationAlgorithmItems[2])

        return sklearn.svm.SVC(kernel=kernel, probability=True, cache_size=200, verbose=False, random_state=0, C=C, gamma=gamma)

    if classificationAlgorithm == "naivebayes":
        return sklearn.naive_bayes.GaussianNB()
    if classificationAlgorithm == "knn":
        return sklearn.neighbors.KNeighborsClassifier()
    if classificationAlgorithm == "decisiontree":
        return sklearn.tree.DecisionTreeClassifier()
    if classificationAlgorithm == "dummy":
        return sklearn.dummy.DummyClassifier()
    if classificationAlgorithm == "randomforest":
        return sklearn.ensemble.RandomForestClassifier(class_weight="auto")

    print "Invalid classifier: %s" % classificationAlgorithm
    exit(1)

def predictForTarget(target):
    binaryTargets = numpy.array([(0, 1)[x == target] for x in targets])

#    mean_tpr = 0.0
#    mean_fpr = numpy.linspace(0, 1, 100)

    allProbs = numpy.array([])
    allTestTargets = numpy.array([])

    for trainIndices, testIndices in cv:
        trainData = data[trainIndices,]
        trainTargets = binaryTargets[trainIndices]
        testData = data[testIndices,]
        testTargets = binaryTargets[testIndices]

        model = classifier.fit(trainData, trainTargets)
        probs = model.predict_proba(testData)[:,1]

        allTestTargets = numpy.concatenate([allTestTargets, testTargets])
        allProbs = numpy.concatenate([allProbs, probs])

#        fpr, tpr, thresholds = sklearn.metrics.roc_curve(testTargets, probs)
#        mean_tpr += scipy.interp(mean_fpr, fpr, tpr)
#        mean_tpr[0] = 0.0
#        roc_auc = sklearn.metrics.auc(fpr, tpr)

#    mean_tpr /= len(cv)
#    mean_tpr[-1] = 1.0

#    return sklearn.metrics.auc(mean_fpr, mean_tpr)

    return sklearn.metrics.roc_auc_score(allTestTargets, allProbs), list(allProbs)

data = [[y for y in line.rstrip().split("\t")] for line in file(dataFilePath)]
samples = data.pop(0)
targets = data.pop(-1)
data = [[float(y) for y in x] for x in data]
data = numpy.transpose(numpy.array(data))

data = sklearn.preprocessing.StandardScaler().fit_transform(data)

if numFolds == "n":
    cv = sklearn.cross_validation.LeaveOneOut(n=len(targets), indices=True)
else:
    cv = sklearn.cross_validation.StratifiedKFold(numpy.array(targets), n_folds=int(numFolds), indices=True)

classifier = getClassifier()

auc, predictions = predictForTarget(targetClass)
metricOutput = "AUC\n" + ("%.8f\n" % auc)

outMetricFile = open(outMetricFilePath, 'w')
outMetricFile.write(metricOutput)
outMetricFile.close()

predictionsOutputList = []
for trainIndices, testIndices in cv:
    for testIndex in testIndices:
        predictionsOutputList.append("%s\t%.8f" % (samples[testIndex], predictions.pop(0)))

outPredictionsFile = open(outPredictionsFilePath, 'w')
outPredictionsFile.write("\n".join(predictionsOutputList))
outPredictionsFile.close()

numTasks = len(glob.glob(os.path.dirname(dataFilePath) + "/*"))
numCompleted = len(glob.glob(os.path.dirname(outMetricFilePath) + "/*"))

print "Approximately %i/%i gene sets complete (%s)" % (numCompleted, numTasks, description)
